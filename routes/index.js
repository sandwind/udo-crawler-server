var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

router.get('/download', function (req, res, next) {
    console.log('response will be sent by the next function ...');
    res.download(__dirname + '/../public/images/favicon.ico');
});

module.exports = router;
